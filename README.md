## LICENSE AND COPYRIGHT
This software is Copyright (c) 2019,2020,2021,2022,2023 by the National Research Council of Canada (NRC) but is freely available for use without any warranty under the GNU GENERAL PUBLIC LICENSE (Version 3, 29 June 2007). Refer to wrapped tools for their credits and license information.
This license does not grant you the right to use any trademark, service mark, tradename, or logo of the Copyright Holder.
This license includes the non-exclusive, worldwide, free-of-charge patent license to make, have made, use, offer to sell, sell, import and otherwise transfer the Package with respect to any patent claims licensable by the Copyright Holder that are necessarily infringed by the Package. If you institute patent litigation (including a cross-claim or counterclaim) against any party alleging that the Package constitutes direct or contributory patent infringement, then this Artistic License to you shall terminate on the date that such litigation is filed.
Disclaimer of Warranty: THE PACKAGE IS PROVIDED BY THE COPYRIGHT HOLDER AND CONTRIBUTORS "AS IS' AND WITHOUT ANY EXPRESS OR IMPLIED WARRANTIES. THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT ARE DISCLAIMED TO THE EXTENT PERMITTED BY YOUR LOCAL LAW. UNLESS REQUIRED BY LAW, NO COPYRIGHT HOLDER OR CONTRIBUTOR WILL BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING IN ANY WAY OUT OF THE USE OF THE PACKAGE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

## Summary
The NRC's pipeline's (this repository) core libraries were forked from the GenPipes (https://doi.org/10.1101/459552) repository (formely the mugqic_pipeline repository) on August 16th 2014. This repository holds two pipelines: 1) AmpliconTagger [https://doi.org/10.1093/gigascience/giz146] and 2) ShotgunMG [https://doi.org/10.1101/2022.04.19.488797] pipelines for the moment. 
AmpliconTagger is a pipeline designed to process short amplicon sequencing marker or functional gene sequencing data. ShotgunMG is designed to process shotgun metagenomics and shotgun metatranscriptomic sequencing data.


## Requirements
The software from this repository requires Python, Perl and R and has been tested with the following versions:
Python/3.9.0; Perl/5.26.0; R/4.2.1

This pipeline infrastructure relies on its companion nrc_tools_public repository (https://bitbucket.org/jtremblay514/nrc_tools_public).

The pipeline will generate jobs that will need to have a proper installation of the following softwares using the Environment Module System (Lmod).

Software installation scripts are available in the nrc_resources_public repository.
https://bitbucket.org/jtremblay514/nrc_resources_pubic


## Reference and help
We provide a CentOS-7 image that contains all that is needed to AmpliconTagger and ShotgunMG. It contains the third party softwares, modules, databases and a subset of sequencing libraries for testing purposes.

**An exhaustive user guide for AmpliconTagger is available here: [User Guide](http://jtremblay.github.io/amplicontagger_guide_v1.3.2.html)**

**and available here for ShotgunMG: [User Guide](http://jtremblay.github.io/shotgunmg_guide_v1.3.2.html)**

## Pipeline diagram
http://jtremblay.github.io/PipelineViewer/amplicontagger.html

http://jtremblay.github.io/PipelineViewer/shotgunmg.html
